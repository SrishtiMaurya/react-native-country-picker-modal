import React from 'react'
import { create } from 'react-test-renderer'
import CountryPicker from '../src/'

// Mock react-test-renderer
jest.mock('react-test-renderer', () => ({
  create: jest.fn().mockReturnValue({
    toTree: jest.fn(),
  }),
}))

describe('CountryPicker', () => {
  it('renders without errors', () => {
    const picker = create(
      <CountryPicker countryCode={'US'} onSelect={() => {}} />,
    )
    expect(picker).toBeDefined()
  })
})
