#!/usr/bin/env node

const fs = require('fs')

const [_, __, path] = process.argv
const newPath = './lib/data/countries-emoji.json' // Change this to the new path

const parse = JSON.parse
const stringify = data => JSON.stringify(data, null, '')
const minify = newPath => stringify(parse(fs.readFileSync(newPath, 'utf-8')))
const save = (newPath, contentString) =>
  fs.writeFileSync(newPath, contentString, { encoding: 'utf8', flag: 'w' })

save(newPath, minify(newPath))
